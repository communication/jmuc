/*
 * jmuc - jabber multi user chat client 
 *
 * Copyright (C) 2006 Christian Dietrich <stettberger@gmx.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

#define _GNU_SOURCE 1

#ifndef _JMUC_H
#define _JMUC_H 1

#include <loudmouth/loudmouth.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include <unistd.h>




typedef enum {
  AFFILIATION_OWNER,
  AFFILIATION_ADMIN,
  AFFILIATION_MEMBER,
  AFFILIATION_NONE
} Affiliation;
  
typedef enum {
  ROLE_MODERATOR,
  ROLE_PARTICIPANT,
  ROLE_VISITOR,
  ROLE_NONE
} Role;

typedef enum {
  STATUS_AVAILABLE,
  STATUS_AWAY,
  STATUS_CHAT,
  STATUS_XA,
  STATUS_DND,
  STATUS_NONE
} Status;

 /* An MUC Buddy */
struct _List{
  void *data;
  struct _List *next;
} ; 

typedef struct _List List;

typedef struct {
  char *username;
  Affiliation aff;
  Role role;
  Status status;  
  char *away;
} Buddy;

typedef struct {
  char *name;
  char *nick;
  int fd;
  int gsource_tag;
  List *buddies;
} Channel;




/* Encoding */
char *to_utf8(const char *str);
char *from_utf8(const char *str);
int encoding_init();

gboolean  handle_channel_input(GIOChannel   *source,
			       GIOCondition  condition,
			       gpointer      data);


Role str2role(char *str);
Affiliation str2aff(char *str);
char aff2char(Affiliation aff);
Status node2status (LmMessageNode *node);
char *status2str(Status status);
void splitjid(char *jid, char **room, char **user);
char *get_username_from_jid(char *jid);
LmMessageNode *get_x_with_xmlns(LmMessageNode *source, char *xmlns);

void add_channel(char *name, char *nick);
void rm_channel(char *name);
void add_buddy(LmMessageNode *presence);
void rm_buddy(LmMessageNode *presence);
List *list_append(List *list, void *data);
List *list_remove(List *list, void *data);

Channel *get_channel_by_name(char *str);
Buddy *get_buddy_by_name(Channel *ch, char *str);
void make_dirs(char *path);
char *make_filepath(char *channel, char *file);
int open_file_as_fifo(char *channel) ;
void print_out(char *channel, char *buf, ...);
void print_out_stamp(char *channel, char *stamp, char *buf, ...);
void DEBUG(char *fmt, ...);



#define NULL_TEST(a) if(!(a)){perror("jmuc: Couldn't allocate memory"); return;}
#define NULL_TEST_ARG(a,b) if(!(a)){perror("jmuc: Couldn't allocate memory"); return (b);}


#endif
