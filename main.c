#define _GNU_SOURCE 1

#include "jmuc.h"



const char *username="tester";
const char *password="tester";
char *resource="jmuc";
const char *server="jabber.zerties.org";
char *serverpath=NULL;

typedef struct {
  char *username;
  char *password;
  char *resource;
  char *server;
} ConnectData;

static GMainLoop *main_loop;
LmConnection *connection;
GMainContext *context;
List *channels=NULL;



LmHandlerResult chat_message_cb  (LmMessageHandler *handler,
				  LmConnection *connection,
				  LmMessage *m,
				  gpointer user_data) {
  LmMessageNode *body=lm_message_node_get_child (m->node, "body"), *x;
  char *room, *user;
  splitjid((char *)lm_message_node_get_attribute(m->node, "from"), &room, &user); 

  if (!body) {
    DEBUG("Error: Message had no body");
    return LM_HANDLER_RESULT_REMOVE_MESSAGE;
  }

  if (!strcmp("groupchat", lm_message_node_get_attribute  (m->node, "type"))) {
    if (user){
      if (!(x=get_x_with_xmlns(m->node, "jabber:x:delay")))
	print_out(room, "<%s> %s", user ,lm_message_node_get_value(body));
      else
	print_out_stamp(room, (char *)lm_message_node_get_attribute(x, "stamp"), "<%s> %s", 
			user, lm_message_node_get_value(body));
    }
    else {
      if (!(x=get_x_with_xmlns(m->node, "jabber:x:delay")))
	print_out(room, "-!- %s", lm_message_node_get_value(body));
      else
	print_out_stamp(room, (char *)lm_message_node_get_attribute(x, "stamp"), "-!- %s", 
			lm_message_node_get_value(body));
    }
  }
  else {

      print_out("", "<%s> %s", lm_message_node_get_attribute(m->node, "from"),
		lm_message_node_get_value(body));
  }
  free(room);
  lm_message_node_unref ( body);
  return LM_HANDLER_RESULT_REMOVE_MESSAGE;
}



LmHandlerResult presence_message_cb  (LmMessageHandler *handler,
				      LmConnection *connection,
				      LmMessage *m,
				      gpointer user_data) {
  if(get_x_with_xmlns(m->node, "http://jabber.org/protocol/muc#user")) {
    if(lm_message_node_get_attribute(m->node, "type")){
      if(!strcmp(lm_message_node_get_attribute(m->node, "type"), "unavailable")) {
	rm_buddy(m->node);
      }
    }
    else 
      add_buddy(m->node);
  }
  return LM_HANDLER_RESULT_REMOVE_MESSAGE;
}



LmHandlerResult join_room_cb  (LmMessageHandler *handler,
				  LmConnection *connection,
				  LmMessage *m,
				  char *jid) {
  LmMessageNode *error;
  error=lm_message_node_get_child    (m->node,"error");

  if (error){
    print_out("", "-!- Error: %s %s", lm_message_node_get_attribute(error, "code"), lm_message_node_get_value(error));
    if (!strcmp(lm_message_node_get_attribute(error, "code"), "409")) /* Please choose a different Nick */ {
      jid=realloc(jid, strlen(jid)+2);
      NULL_TEST_ARG(jid, LM_HANDLER_RESULT_REMOVE_MESSAGE);
      strcat(jid, "_");
      Channel *c=get_channel_by_name((char *)lm_message_node_get_attribute(m->node, "from"));
      if (!c) {
	perror("jmuc: Fatal Error");
	return LM_HANDLER_RESULT_REMOVE_MESSAGE;
      }
      free(c->nick);
      c->nick=strdup(get_username_from_jid(jid));

      /* Making a new join message */
      LmMessage *presence;
      LmMessageNode *x;
      presence=lm_message_new (jid, LM_MESSAGE_TYPE_PRESENCE);
      
      lm_message_node_set_attribute(presence->node, "from", lm_connection_get_jid(connection));
      x=lm_message_node_add_child    (presence->node, "x", "");
      lm_message_node_set_attribute(x, "xmlns", "http://jabber.org/protocol/muc");

      lm_connection_send_with_reply (connection, presence, handler, NULL);
      print_out("", "-!- Your new Nickname is %s", jid);
      lm_message_unref (presence);
    }
    else rm_channel((char *)lm_message_node_get_attribute(m->node, "from"));
  }
  else 
    free(jid);
  return LM_HANDLER_RESULT_REMOVE_MESSAGE;
}

static void 
join_room(char *jid, char *nick) {
  char *to;
  asprintf(&to, "%s/%s", jid, nick);

  LmMessage *presence;
  LmMessageNode *x;
  presence=lm_message_new (to, LM_MESSAGE_TYPE_PRESENCE);

  lm_message_node_set_attribute(presence->node, "from", lm_connection_get_jid(connection));
  x=lm_message_node_add_child    (presence->node, "x", "");
  lm_message_node_set_attribute(x, "xmlns", "http://jabber.org/protocol/muc");

  LmMessageHandler *join_message = lm_message_handler_new((LmHandleMessageFunction) join_room_cb,
							  to, g_free);
  lm_connection_send_with_reply (connection, presence, join_message, NULL);
  /* Add channel */
  add_channel(jid, nick);
  lm_message_unref (presence);
}

static void
connection_auth_cb (LmConnection *connection, 
                    gboolean      success, 
                    ConnectData  *data)
{
  if (!success) {
    g_error ("Authentication failed");
  }
  /* Add Message Handler */
  LmMessageHandler *chat_message= lm_message_handler_new(chat_message_cb, NULL, g_free);
  lm_connection_register_message_handler(connection, chat_message, LM_MESSAGE_TYPE_MESSAGE,
					 LM_HANDLER_PRIORITY_NORMAL);
  chat_message= lm_message_handler_new(presence_message_cb, NULL, g_free);
  lm_connection_register_message_handler(connection, chat_message, LM_MESSAGE_TYPE_PRESENCE,
					 LM_HANDLER_PRIORITY_NORMAL);

  join_room("zerties@conference.jabber.zerties.org", "stettberger");
  add_channel("", NULL);
}

void   disconnected_cb(LmConnection *connection,
		       LmDisconnectReason reason,
		       gpointer user_data){
  switch(reason) {
  case LM_DISCONNECT_REASON_PING_TIME_OUT:
    print_out("", "-!- You left network[Ping timeout]");    
  case LM_DISCONNECT_REASON_HUP:
    print_out("", "-!- You left network[Lost connection]");    
  case LM_DISCONNECT_REASON_ERROR:
    print_out("", "-!- You left network[Generic error]");    
  default:
    print_out("", "-!- You left network[]");    
  }
  exit(0);
}

void connection_open_result_cb (LmConnection *connection,
                           gboolean      success,
                           ConnectData  *data)
{
  GError *error = NULL;
  if (!success) {
    g_error ("Connection failed");
  }
  if (!lm_connection_authenticate (connection, data->username,
				   data->password, data->resource,
				   (LmResultFunction) connection_auth_cb,
				   NULL,
				   g_free,
				   &error)) {
    g_error ("lm_connection_authenticate failed");
  }

}


int 
main () {


  guint         port = LM_CONNECTION_DEFAULT_PORT;

  GError       *error = NULL;

  /* Encoding */
  encoding_init();

  context = g_main_context_new ();
  connection = lm_connection_new_with_context (server, context);

  ConnectData *connect_data;
  connect_data = g_new0 (ConnectData, 1);
  connect_data->username = (char *)username;
  connect_data->password = (char *)password;
  connect_data->resource = resource;
  connect_data->server = (char *)server;
  serverpath="/home/stettberger/irc/stettberger@jabber.ccc.de";

  /* Set Jid */
  char *jid;
  asprintf(&jid, "%s@%s/%s", username, server, resource);
  lm_connection_set_jid(connection, jid);
  free(jid);

  /* Set Port */
  lm_connection_set_port(connection, port);

  /* Set Disconnected Function */
  lm_connection_set_disconnect_function(connection, disconnected_cb, NULL, g_free);
          
  if (!lm_connection_open (connection, 
			   (LmResultFunction) connection_open_result_cb,
			   connect_data,
			   g_free,
			   &error)) {
    g_error ("lm_connection_open failed");
  }

  main_loop = g_main_loop_new (context, FALSE);
  g_main_loop_run (main_loop);
  
  return 0;
}
