/*
 * jmuc - jabber multi user chat client 
 *
 * Copyright (C) 2006 Christian Dietrich <stettberger@gmx.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

#include "jmuc.h"

extern List *channels;
extern GMainContext *context;
static const int debug=1;
extern char *serverpath;

void DEBUG(char *fmt, ...)
{
  if(! debug) return;
  va_list va;
  va_start(va, fmt);
  vfprintf(stderr,fmt, va);
  putchar('\n');
  va_end(va);
}

Role str2role(char *str) {
  if(!str) return ROLE_NONE;
  if (!strcmp("moderator", str)) 
    return ROLE_MODERATOR;
  if (!strcmp("participant", str)) 
    return ROLE_PARTICIPANT;
  if (!strcmp("visitor", str))
    return ROLE_VISITOR;
  else
    return ROLE_NONE;
}
Affiliation str2aff(char *str) {
  if(!str) return AFFILIATION_NONE;
  if (!strcmp("owner", str)) 
    return AFFILIATION_OWNER;
  if (!strcmp("admin", str)) 
    return AFFILIATION_ADMIN;
  if (!strcmp("member", str)) 
    return AFFILIATION_MEMBER;
  else
    return AFFILIATION_NONE;
}
char aff2char(Affiliation aff) {
  switch(aff){
  case AFFILIATION_OWNER:
    return '*';
  case AFFILIATION_ADMIN:
    return '@';
  default:
    return ' ';
  }
}

Status node2status (LmMessageNode *node) {
  if (!node) 
    return STATUS_AVAILABLE;
  const char *value=lm_message_node_get_value (node);
  if(!strcmp("away", value)) 
    return STATUS_AWAY;
  if (!strcmp("chat", value)) 
    return STATUS_CHAT;
  if (!strcmp("xa", value)) 
    return STATUS_XA;
  if (!strcmp("dnd", value)) 
    return STATUS_DND;
  else 
    return STATUS_NONE;    
}
char *status2str(Status status){
  char *stat=NULL;
  switch(status) {
  case STATUS_AVAILABLE:
    stat=strdup("online");
    break;
  case STATUS_AWAY:
    stat=strdup("away");
    break;
  case STATUS_CHAT:
    stat=strdup("free for chat");
    break;
  case STATUS_XA:
    stat=strdup("extended away");
    break;
  case STATUS_DND:
    stat=strdup("do not disturb");
    break;
  case STATUS_NONE:
    stat=strdup("none (error?)");
    break;
  }
  NULL_TEST_ARG(stat, strdup("ERROR"));
  return stat;
}

void splitjid(char *jid, char **room, char **user) {
  *room=strdup(jid);
  *user=NULL;
  if (strchr(jid, '/')) { /* No User */
    *user=strchr(*room, '/');
    **user=0;
    (*user)++;
  }
}
char *get_username_from_jid(char *jid){
  char *p;
  if(!(p=strchr(jid, '/')))
    return jid;
  else
    return ++p;
}

int open_file_as_fifo(char *channel) {
  char *dir=make_filepath(channel, "in");
  if (!dir){
    perror("jmuc: Fatal Error in open_file_as_fifo: Filepath");
    return -1;
  }
  char *p=strrchr(dir, '/');
  *p=0;
  make_dirs(dir);
  *p='/';
  if (access(dir, F_OK)==-1)
    mkfifo(dir, S_IRWXU);
  int file=open(dir, O_RDONLY|O_NONBLOCK, 0);
  if(file==-1) {
    perror("jmuc: Fatal Error in open_file_as_fifo");
    return -1;
  }
  free(dir);
  return file;
}

void add_channel(char *name, char *nick) {
  Channel *ch;
  if (get_channel_by_name(name))
    return;
  ch=calloc(1, sizeof(*ch));
  NULL_TEST(ch);
  ch->name=strdup(name);
  NULL_TEST(ch->name);
  ch->fd=open_file_as_fifo(name);
  if(ch->fd==-1) {
    printf("jmuc: Couldn't open the infile");
    return;
  }
  if(nick){
    ch->nick=strdup(nick);
    NULL_TEST(ch->nick);
  }
  else ch->nick=NULL;
  channels=list_append(channels, ch);
  
  /* Watch */
  GIOChannel *io_channel= g_io_channel_unix_new(ch->fd);
  GSource *source=g_io_create_watch(io_channel, G_IO_IN);
  g_source_set_callback (source, (GSourceFunc)handle_channel_input, (gpointer)ch, g_free);
  ch->gsource_tag=g_source_attach(source, context);
  if (*name!=0)
    print_out("", "-!- You joined channel: %s", name);
  DEBUG("Append Channel: %s", name);
}

void rm_channel(char *name) {
  Channel *ch=get_channel_by_name(name);
  GSource *source;
  List *b; 
  Buddy *bd;

  if (!ch)
    DEBUG("Error: No such channel \"%s\"", name);
  channels=list_remove(channels, ch);
  /* Free the Channel */
  free(ch->name);
  close(ch->fd);
  
  source=g_main_context_find_source_by_id(context, ch->gsource_tag);
  if (source) g_source_destroy(source);
  
  for(b=ch->buddies; b; b=b->next) {
    bd=(Buddy *)b->data;
    free(bd->username);
    free(bd->away);
    free(bd);
    free(b);
  }
  free(ch);
  DEBUG("Removed Channel: %s", name);
}

void add_buddy(LmMessageNode *presence) {
  Channel *ch;
  Buddy *bd;
  short exist=TRUE;
  char *room, *user;
  LmMessageNode *x, *item, *status;
  splitjid((char *)lm_message_node_get_attribute(presence, "from"), &room, &user);
  if(!user) {
    free(room);
    return;
  }
  if(!(ch=get_channel_by_name(room))) {
    print_out("", "-!- We got an presence Message from an unjoined channel");
    return;
  }

  if(!get_buddy_by_name(ch, user)) {
    bd=calloc(1, sizeof(*bd));
    NULL_TEST(bd);
    exist=FALSE;
  }
  else 
    bd=get_buddy_by_name(ch, user);

  x=get_x_with_xmlns(presence, "http://jabber.org/protocol/muc#user");
  if(!x) return;
  if(!(item=lm_message_node_get_child(x, "item"))) return;
  bd->username=strdup(user);
  NULL_TEST(bd->username);
  bd->aff=str2aff((char *)lm_message_node_get_attribute(item, "affiliation"));
  bd->role=str2role((char *)lm_message_node_get_attribute(item, "role"));
  bd->status=node2status(lm_message_node_get_child(presence, "show"));
  if ((status=lm_message_node_get_child(presence, "status")))
      bd->away=strdup((char *)lm_message_node_get_value(status));
  if(!exist){
    ch->buddies=list_append(ch->buddies, bd);
    print_out(room, "-!- %s has joined room", user);
    DEBUG("Append User: %s", user);
  }
  else {
    char *mode=status2str(bd->status);
    print_out(room, "-!- %s has changed his mode to: %s", user, mode);
    free(mode);
  }
  

}

void rm_buddy(LmMessageNode *presence) {
  char *room, *user;
  Channel *ch;
  splitjid((char *)lm_message_node_get_attribute(presence, "from"), &room, &user);
  if(!user) {
    free(room);
    return;
  }
  if(!(ch=get_channel_by_name(room))) return;
  Buddy *bd;
  if(!(bd=get_buddy_by_name(ch, user))) return;
  print_out(room, "-!- %s has left the channel", user);

  ch->buddies=list_remove(ch->buddies, bd);
  free(bd->username);
  free(bd->away);
  free(bd);
  DEBUG("Removed User: %s", user);
}



Buddy *get_buddy_by_name(Channel *ch, char *str) {
  List *c;
  Buddy *bd;
  if(!ch) return NULL;
  c=ch->buddies;
  for (c=ch->buddies; c; c=c->next){
    bd=c->data;
    if(!strcmp(bd->username, str)) 
      return bd;
  }
  return NULL;
}

Channel *get_channel_by_name(char *str) {
  List *c;
  Channel *ch;
  c=channels;
  for (c=channels; c; c=c->next){
    ch=c->data;
    if(!strcmp(ch->name, str)) 
      return ch;
  }
  return NULL;
}

List *list_append(List *list, void *data) {
  List *new, *old=list;

  new=calloc(1, sizeof(*new));
  NULL_TEST_ARG(new, NULL);
  new->data=data;
  new->next=NULL;
  if(!old) 
    old=new;
  else {
    for (;list->next;list=list->next);
    list->next=new;
  }
  return old;
}

List *list_remove(List *list, void *data) {
  List *tmp, *tmp2;
  if (!list) return NULL;
  if (list->data == data ) {
    free(list);
    return NULL;
  }
  for (tmp=list; tmp && tmp->next; tmp=tmp->next) {
    if (tmp->next->data == data ){
      tmp2=tmp->next;
      tmp->next=tmp2->next;
      free(tmp2);
    }
  }
  return list;
}

void make_dirs(char *arg) {
  char *path=strdup(arg);
  char *p=path;
  if (*p=='/') p++;
  while( (p=strchr(p, '/')) ) {
    p[0]=0;
    mkdir(path, S_IRWXU);
    p[0]='/';
    p++;
  }
  mkdir(path, S_IRWXU);
  free(path);
}


/*Takes an non utf-8 string and writes it out */
void _print_out(char *channel, char *message) {
  char *dir, *p, *utf8;
  FILE *out;
  dir=make_filepath(channel, "out");
  p=strrchr(dir, '/');
  if (!p) perror("jmuc: fatal error");
  *p=0;
  make_dirs(dir);
  *p='/';
  if (!(out=fopen(dir, "a"))) {
    perror("jmuc: Couldn't open file");
    return;
  }
  utf8=from_utf8(message);
  fprintf(out, "%s",utf8);
  fclose(out);
  
  free(dir);
}

void print_out_stamp(char *channel, char *stamp, char *buf, ...) {
  static char buft[8];
  char *msg, *message, *p;

  va_list va;
  va_start(va, buf);
  vasprintf(&msg, buf, va);

  /* Timestamp */
  if(!(p=strchr(stamp, 'T'))) 
    p=stamp;
  else
    p++;
  strncpy(buft, p, 5);

  asprintf(&message, "%s %s\n",buft, msg);
  _print_out(channel, message);
  free(msg);
  free(message);
  va_end(va);
}

void print_out(char *channel, char *buf, ...) {
  static char buft[8];
  time_t t = time(0);
  char *msg, *message;

  va_list va;
  va_start(va, buf);
  vasprintf(&msg, buf, va);
  strftime(buft, sizeof(buft), "%R ", localtime(&t));
  asprintf(&message, "%s%s\n",buft, msg);
  _print_out(channel, message);
  free(msg);
  free(message);
  va_end(va);
}

/* Returns a channel string like: $serverdir/<$cannel>/$file */
char *make_filepath(char *channel, char *file) {
  char *result;
  if (!channel || *channel==0) {
    if (!asprintf(&result, "%s/%s", serverpath, file)) 
      return NULL;
  }
  else {
    if (!asprintf(&result, "%s/%s/%s", serverpath, channel, file)) 
      return NULL;
  }
  return result;
}

LmMessageNode *get_x_with_xmlns(LmMessageNode *source, char *xmlns) {
  LmMessageNode *x;
  char *tmp;
  if (!(x=lm_message_node_get_child(source, "x"))) 
    return NULL;
  while(1){
    if (!x) return NULL;
    else if (!(tmp=(char *)lm_message_node_get_attribute(x, "xmlns"))
        || strcmp(tmp, xmlns)) 
      x=x->next;
    else
      return x;
  }
  return NULL;
}


