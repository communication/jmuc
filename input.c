#include "jmuc.h"

extern LmConnection *connection;

void proc_channel_input(Channel *ch, char *msg);

gboolean  handle_channel_input(GIOChannel   *source,
			       GIOCondition  condition,
			       gpointer      data) {
  DEBUG("INPUT: %s", ((Channel *) data)->name);
  Channel *ch=(Channel *) data;
  FILE *stream=fdopen(ch->fd, "r");
  if (!stream) {
    perror("jmuc: Fatal Error");
    return FALSE;
  }
  char *message=malloc(PIPE_BUF);
  message=fgets(message, PIPE_BUF, stream);
  if (!message) return TRUE;
  if(message[strlen(message)-1]=='\n')
    message[strlen(message)-1]=0;
  proc_channel_input(ch, message);
  g_free(message);
  return TRUE;
}

void proc_channel_input(Channel *ch, char *msg) {
  if (*msg=='/'){
    if (!strncmp(&msg[1], "names", 5)) {
      if (*ch->name==0) {
	print_out("", "-!- There is no user in the main channel ;) ");
      }
      else {
	char *users=malloc(strlen(ch->nick)+3);
	NULL_TEST(users);
	sprintf(users, "[%s] ", ch->nick);

	List *b;
	Buddy *bd;
	for(b=ch->buddies; b; b=b->next){
	  bd=b->data;
	  char *user=malloc(strlen(bd->username)+5);
	  NULL_TEST(user);
	  sprintf(user, "[%c%s] ", aff2char(bd->aff), bd->username);
	  users=realloc(users, strlen(users)+strlen(user)+1);
	  NULL_TEST(users);
	  users=strcat(users, user);
	  free(user);
	}
	print_out(ch->name, users);
	free(users);
      }
    }
    /* commands */
  }
  else {
    if (ch->name=="") 
      print_out("", "-!- This is not a real channel");
    else {
      char *utf8=to_utf8(msg);
      LmMessage *message;
      LmMessageNode *body;
      message=lm_message_new (ch->name, LM_MESSAGE_TYPE_MESSAGE);
      lm_message_node_set_attribute(message->node, "type", "groupchat");
      body=lm_message_node_add_child(message->node, "body", utf8);
      if (!lm_connection_is_authenticated (connection) || 
	  ! lm_connection_is_open(connection)) {
	print_out("", "-!- No connected to Server");
	return;
      }
      lm_connection_send (connection, message, NULL);
      lm_message_unref(message);
    }
  }
}
