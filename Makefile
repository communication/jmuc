CC=gcc
prefix=/usr/local
DESTDIR=
TARGET=jmuc
SRCS=main.c tools.c encoding.c input.c
OBJS=main.o tools.o encoding.o input.o
HEADERS=jmuc.h
CFLAGS=`pkg-config --cflags loudmouth-1.0` -ggdb -Wall
LDFLAGS=`pkg-config --libs  loudmouth-1.0`


${TARGET}:${OBJS} ${HEADERS}
	${CC} ${OBJS} ${LDFLAGS} -o $@
.c.o:
	${CC} -c $< -o $@ ${CFLAGS}
clean:
	rm -rf *.o *~ core ${TARGET}

